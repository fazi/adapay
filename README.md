# AdaPay SDK

## 概述

本包封装了AdaPay官方发布的SDK。

## 安装

- 运行 ***composer*** 命令:

        composer require fazi/adapay

   或者在根目录的 `composer.json` 文件中添加：

        "require": {
            "fazi/adapay": "^0.1"
        }
        
   然后运行命令 `composer install` 安装依赖。
   
## 后言
欢迎访问我的个人主页 https://www.fazi.me/
